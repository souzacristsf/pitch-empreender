#HSLIDE
#### <span style="color: #e49436">Uma breve história</span>
<br>
<!--[Mockup](https://xd.adobe.com/view/73db159f-14fc-48f7-bd05-09e04bf89955/screen/dcb706fe-9cfe-4be1-ac2c-ce409e001f8f)--> 

#HSLIDE
#### <span style="color: #e49436">João e sua mulher Jovelina</span>
<span style="color: #e49436">João</span> 
e sua mulher costumam fazer exercicios diariamente.
</br>
</br>
![Logo](images/passeio.jpg)
</br>

#HSLIDE
#### <span style="color: #e49436">Check-up</span>
Em um certo dia João resolveu fazer um check-up. 
</br>
E foi diagnosticado com <span style="color: #e49436">Câncer</span>
</br>
![Logo](images/doente.png)

#HSLIDE
Já não basta sofrer com os efeitos colaterais da quimioterapia. 
João, tem que aguardar horas na fila para realizar o procedimento. 

![LOGO](images/fila.jpg)


#HSLIDE
## <span style="color: #e49436">Solução</span>

<!--#### Get the word out
<br>
<span style="color:gray">Markdown Presentations For Developers</span>
<br>
<span style="color:gray">on</span>
<br>
<span style="color:gray">GitHub, GitLab and Bitbucket</span>-->

#HSLIDE

### <span style="color: #e49436; text-transform: none">Plataforma de Gerenciamento de Fila Virtual</span>
+ Aplicativo "Pré-Atendimento"
+ Agendar Consultas ou Exames
+ Retirar Senha
+ Acompanhar sua posição na fila em tempo real
+ Informação do tempo chegada entre residencia e hospital "Geolocalização".  
+ Atendimento Via Telefone

<!--<span style="color:gray;">[ JUST LIKE THIS ONE ]</span>-->

#HSLIDE
### <span style="color: #e49436; text-transform: none">Mercado</span>
HOSPITAIS DO BRASIL
<table>
  <tr>
    <th>Setor</th>
    <th>%</th> 
  </tr>
  <tr>
    <td>Municipais</td>
    <td>21%</td>
  </tr>
  <tr>
    <td>Estaduais</td>
    <td>8%</td>
  </tr>
  <tr>
    <td>Federais</td>
    <td>1%</td>
  </tr>
  <tr>
    <td>Privados</td>
    <td>70%</td>
  </tr>
  <tr>
    <td>Total</td>
    <td>6.761</td>
  </tr>
</table>

#HSLIDE
</br>
<img src="images/concorrencia.jpg" style="width: 30%;"></img>
</br>
Analisa permanência em Fila.

<img src="images/concorrencia1.png" style="width: 30%;"></img>
</br>
Gerenciamento de Fila

<img src="images/concorrencia2.png" style="width: 30%;"></img>
</br>
Tecnologia voltada para o controle de atendimento ao público por meio de filas de atendimento.

#HSLIDE
# <span style="color: #e49436">R$?</span>
Implementação + Mensalidade
<br>

#HSLIDE
# <span style="color: #e49436">R$?</span>
Implementação + Mensalidade
<br>
